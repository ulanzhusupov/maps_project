package com.example.maps_project
import com.yandex.mapkit.MapKitFactory

import io.flutter.embedding.android.FlutterActivity

class MainActivity: FlutterActivity() {
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        MapKitFactory.setApiKey("9e7376e0-34fb-4e7a-ac4b-af079c654d48");
        super.configureFlutterEngine();
    }
}
